# Base image, ta dùng để tạo custom image
FROM node:8-alpine
# Thư mục làm việc
WORKDIR /usr/src/app
# Copy file ở máy thật vào image
COPY ["package.json", "./"]
# Chạy một số lệnh cần thiết như cài một số thư viện còn thiếu.
RUN npm install
COPY . .
# Mở port 3000 cho docker
EXPOSE 3000

# Lệnh mặc định khi chạy docker
CMD npm start