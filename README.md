# Hướng dẫn cài đặt và sử dụng docker trên linux
## Cài đặt
* Cài docker:
Mở terminal và chạy lệnh sau:
```
curl -fsSL get.docker.com | sudo sh
```
quá trình cài đật sẽ được tự động diễn ra.
Kiểm tra 
```
docker --version
```
Output:
```
Docker version 18.05.0-ce, build f150324
```
* Cài docker-compose:
```
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
```
* Cấp quyền *executable* cho docker-compose:
```
sudo chmod +x /usr/local/bin/docker-compose
```
Kiếm tra:
```
docker-compose --version
```
Output:
```
docker-compose version 1.21.2, build a133471
```

## Sử dụng
### Tìm và tải image phù hợp.

Image docker 2 phần `<repo name>/<image name>:<image tag>`. Nếu image được support  bởi chính docker thì repo name có thể trống. Tag thường là version của image. Mặc dịnh tag là lastest.

Tùy vào ngôn ngũ vả framework mình sử dụng mà tìm image docker cho phù hợp.

VD: Ta sử dụng nodejs 8 và mysql 5.7.
Lên trang [https://hub.docker.com](https://hub.docker.com) đẻ tim image. Sau khi search, ta tìm được image là [node:8-alpine](https://hub.docker.com/_/node/), [mysql:5.7](https://hub.docker.com/_/mysql/)
### Dockerfile

Dockerfile dùng đẻ tạo ra image riêng của mình, dựa trên image có sẵn:
Ví dụ 1 file Dockerfile:

```yml
# Base image, ta dùng để tạo custom image
FROM node:8-alpine
# Thư mục làm việc
WORKDIR /usr/src/app
# Copy file ở máy thật vào image
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
# Chạy một số lệnh cần thiết như cài một số thư viện còn thiếu.
RUN npm install --silent && mv node_modules ../
COPY . .
# Mở port 3000 cho docker
EXPOSE 3000

# Lệnh mặc định khi chạy docker
CMD npm start
```
### docker-compose

Docker-compose dùng để kết hợp các docker image lại với nhau. Ta dùng docker compose để setup môi trường làm việc.
Giả sử, ta cần thiết lập môi trường cho project nodejs 8, sử dụng mysql 5.7 và dùng phpMyAdmin bản mới nhất để quản lí mysql.
Ta tạo file docker-compose.yml  với nội dung như sau:

```yml
version: '3'

services:
  docker-demo-server: #Hostname cho container    
    image: docker-demo #image name
    container_name: demo_server #Container name
    build: . 
    ports:
      - 3000:3000 # Nat port giữa máy thật vs docker container.
  mysql-server:
    image: mysql:5.7
    container_name: demo_mysql
    environment:
      MYSQL_ROOT_PASSWORD: 123456
      MYSQL_DATABASE: demo
      MYSQL_USER: demo
      MYSQL_PASSWORD: 123456
    ports:
      - 3306:3306
  phpadmin:
    image: phpmyadmin/phpmyadmin
    container_name: demo_phpmyadmin
    environment: # Mặc định phpnyadmin sử dụng localhost mysql, nếu phpmyadmin và mysql ở 2 server khác nhau, thì ra phải khai báo dịa chỉ mysql server
      PMA_HOST: mysql-server
    ports:
      - 8080:80 # Sau khi chạy, truy cập địa chỉ localhost:8080 đẻ vào phpmyadmin
  
```
Để chạy ta dùng lệnh:
```
sudo docker-compose up
```

## Gitlab CI

* Tạo file .gitlab-ci.yml với nội dung như sau:

```yml
image: "node:8-alpie"

variables: 
  MYSQL_DATABASE: demo
  MYSQL_PASSWORD: 123456
  MYSQL_ROOT_PASSWORD: 123456
  MYSQL_USER: demo
services: 
  - "mysql:5.7"
  
#Chạy mỗi trước khi chạy task
before_script: 
  - npm install

stages:
  - build
  - test

build:
  stage: build
  script:
    - npm run build

test: 
  stage: test
  script: 
    - npx jest

```

# References:
* [Dockerfile](https://docs.docker.com/engine/reference/builder)
* [Docker compose file](https://docs.docker.com/compose/compose-file/)
* [Gitlab CI](https://docs.gitlab.com/ee/ci/quick_start/)
